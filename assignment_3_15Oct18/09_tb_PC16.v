module tb_PC16;

wire [15:0] OUT;
reg  [15:0] IN;
reg  INC, RST, LOAD, CLK;

mod_PC16 PC16(OUT, IN, LOAD, INC, RST, CLK);

initial
begin
    $dumpfile("09_tb_PC16.vcd");
    $dumpvars(1, tb_PC16);

    // reset
    INC = 0; LOAD = 0;
    RST = 1; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2;
    #10;
    RST = 0;

    // inc test
    INC = 1; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2;
    #5;
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2;
    INC = 0;
    #10;

    // load test
    IN = 1997; #2; LOAD = 1; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2;
    LOAD = 0;
    #10;

    // keep the state test
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2;
    #10;


    // precedence test
    INC = 1; #2; LOAD = 1; #2; RST = 1; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2;
    #10;

    INC = 1; #2; LOAD = 1; #2; RST = 0; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2;
    #10;

    INC = 1; #2; LOAD = 0; #2; RST = 0; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2;
    #10;



end

endmodule
