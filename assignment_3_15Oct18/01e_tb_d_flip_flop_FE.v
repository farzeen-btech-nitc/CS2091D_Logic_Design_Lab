module tb_d_flip_flop_FE;

reg CLK, D;
wire Q, Q_BAR;

mod_d_flip_flop_FE DFF(Q, Q_BAR, D, CLK);

initial
begin
    $dumpfile("01e_tb_d_flip_flop_FE.vcd");
    $dumpvars(1, tb_d_flip_flop_FE);

    CLK = 0; #10;
    D = 0; #10;
    CLK = 1; #10;
    D = 1; #10;
    CLK = 0; #10;
    CLK = 1; #10;
    D = 0; #10;
    CLK = 0; #10;
end

endmodule
