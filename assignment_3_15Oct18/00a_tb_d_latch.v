module tb_d_latch;

reg D, E;
wire Q, Q_BAR;

mod_d_latch d_latch(Q, Q_BAR, D, E);

initial
begin
    $dumpfile("00a_tb_d_latch.vcd");
    $dumpvars(1, tb_d_latch);

    D = 0; E = 0; #10;
    D = 1; E = 0; #10;
    D = 0; E = 1; #10;
    D = 1; E = 1; #10;
    D = 0; E = 0; #10;
    D = 1; E = 0; #10;
    D = 0; E = 1; #10;
    D = 1; E = 1; #10;
end

endmodule

