module tb_binary_cell;

wire OUT;
reg IN, LOAD, CLK;

mod_binary_cell bin_cell(OUT, IN, LOAD, CLK);

initial
begin

    $dumpfile("02_tb_binary_cell.vcd");
    $dumpvars(1, tb_binary_cell);

    //Test LOAD=1, IN=0
    IN = 0; LOAD = 1; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2
    LOAD = 0;
    #10;

    // Test LOAD = 0
    LOAD = 0; #2;
    IN = 1; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0;
    #10;

    // Test LOAD = 1, IN = 1
    LOAD = 1; #2;
    IN = 1; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0;
    #10;

end

endmodule
