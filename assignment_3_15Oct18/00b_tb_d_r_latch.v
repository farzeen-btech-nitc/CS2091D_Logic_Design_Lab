module tb_d_r_latch;

reg D, E, R;
wire Q, Q_BAR;

mod_d_r_latch d_latch(Q, Q_BAR, D, E, R);

initial
begin
    $dumpfile("00b_tb_d_r_latch.vcd");
    $dumpvars(1, tb_d_r_latch);

    // Initialize
    E = 1; #1;
    D = 0; R = 0; #1;
    E = 0;
    #10;

    // E=0, check transparency
    D = 1; #10; D = 0; #10;

    // E=1, check transparency
    E = 1; #1;
    D = 1; #10; D = 0; #10;

    // Held high when E=0
    D = 1; #10; E = 0; #10; D = 0; #10;


    //Check R, when E=0
    R = 1; #10;
    R = 0; #10;

    //Check R, when E,D=1
    E = 1; #1; D = 1; #10;
    R = 1; #10;
    R = 0; #10;


end

endmodule

