module mod_d_r_latch(Q, Q_BAR, D, E, R);
input D, E, R;
output Q, Q_BAR;

// D Latch with Reset
// Reset has higher priority than D
// Reset bypasses Enable gate

wire D_BAR, R_BAR;
wire S_, R_;
wire S_INT, R_INT;

mod_not not00(D_BAR, D);
mod_not not01(R_BAR, R);

mod_and and01(S_INT, D, E);
mod_and and02(R_INT, D_BAR, E);

mod_or or11(R_, R_INT, R);
mod_and and11(S_, S_INT, R_BAR);

mod_nor nor01(Q, R_, Q_BAR);
mod_nor nor02(Q_BAR, S_, Q);

endmodule
