module mod_DFF_RE_SYNC_RST(Q, Q_BAR, D, CLK, R);
output Q, Q_BAR;
input D, CLK, R;

wire CLK_BAR;
mod_not not_clk(CLK_BAR, CLK);

wire Q1, Q1_BAR;

wire R_BAR, Q1_R_BAR, D_R_BAR;

mod_not not_rst(R_BAR, R);
mod_and and_q1_rst_bar(D_R_BAR, R_BAR, D);

mod_d_latch d_latch1(Q1, Q1_BAR, D_R_BAR, CLK_BAR); //Enable = CLK_BAR
mod_d_latch d_latch2(Q, Q_BAR, Q1, CLK);

endmodule
