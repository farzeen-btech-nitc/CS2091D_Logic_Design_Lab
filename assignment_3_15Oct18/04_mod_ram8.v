module mod_ram8(OUT, IN, ADDR, LOAD, CLK);
input [15:0] IN;
input [2:0] ADDR;
input LOAD, CLK;
output [15:0] OUT;

wire [7:0][15:0] OUT_ ;
wire [7:0] LOAD_;

mod_16bit_reg regs[7:0](OUT_, IN, LOAD_, CLK);

mod_8way_demux demux8(LOAD_, LOAD, ADDR);
mod_16bit_8way_mux mux8(OUT, OUT_, ADDR);

endmodule
