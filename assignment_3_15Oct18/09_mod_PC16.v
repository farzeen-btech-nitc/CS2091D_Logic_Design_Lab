module mod_PC16(OUT, IN, LOAD, INC, RST, CLK);
output [15:0] OUT;
input [15:0] IN;
input LOAD, INC, RST, CLK;


// INC_OUT = OUT+1
wire [15:0] INC_OUT;
mod_16bit_incr incr(INC_OUT, , OUT, 1'b1);

// if INC, IN_0 = INC_OUT, else IN_0 = OUT
wire [15:0] IN_0;
mod_16bit_mux incr_mux(IN_0, OUT, INC_OUT, INC);

// if LOAD, IN_1 = IN, else IN_1 = IN_0
wire [15:0] IN_1;
mod_16bit_mux load_mux(IN_1, IN_0, IN, LOAD);

// if RST, IN_2 = 0, else IN_2 = IN_1
wire [15:0] IN_2;
mod_and and1 [15:0] (IN_2, IN_1, ~RST);

// LOAD_INT = LOAD | INC | RST
wire LOAD_INT, LI_;
mod_or or1(LI_, LOAD, INC);
mod_or or2(LOAD_INT, LI_, RST);


mod_16bit_reg reg16(OUT, IN_2, LOAD_INT, CLK);

endmodule
