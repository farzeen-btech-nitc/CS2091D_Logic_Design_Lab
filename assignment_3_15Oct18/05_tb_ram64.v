module tb_ram64;
reg CLK, LOAD;
reg [5:0] ADDR;
reg [15:0] IN;

wire [15:0] OUT;

mod_ram64 ram64(OUT, IN, ADDR, LOAD, CLK);

initial
begin
    $dumpfile("05_tb_ram64.vcd");
    $dumpvars(1, tb_ram64);

    LOAD = 1; #1;
    IN = 16'b0; #1;
    ADDR = 0;
    CLK = 0; #1; CLK = 1; #1; CLK = 0; #1;
    #10;

    LOAD = 0; #1;
    IN = 123; #1;
    CLK = 0; #1; CLK = 1; #1; CLK = 0; #1;
    #10;

    LOAD = 1; #1;
    IN = 123; #1;
    CLK = 0; #1; CLK = 1; #1; CLK = 0; #1;
    #10;

    ADDR = 60; #1;
    IN = 255; #1;
    CLK = 0; #1; CLK = 1; #1; CLK = 0; #1;
    #10;

    ADDR = 0;
    #10;

    ADDR = 60;
    #10;

end

endmodule
