module tb_16bit_reg;

wire [15:0] OUT;
reg  [15:0] IN;
reg  CLK, LOAD;

mod_16bit_reg register(OUT, IN, LOAD, CLK);

initial
begin

    $dumpfile("03_tb_16bit_reg.vcd");
    $dumpvars(1, tb_16bit_reg);

    //Test LOAD=1, IN=0
    IN = 0; LOAD = 1; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0; #2
    LOAD = 0;
    #10;

    // Test LOAD = 0
    LOAD = 0; #2;
    IN = 16'b 1111_1111_1111_1111; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0;
    #10;

    // Test LOAD = 16'b 1111_1111_1111_1111, IN = 16'b 1111_1111_1111_1111
    LOAD = 1; #2;
    IN = 16'b 1111_1111_1111_1111; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0;
    #10;

end

endmodule
