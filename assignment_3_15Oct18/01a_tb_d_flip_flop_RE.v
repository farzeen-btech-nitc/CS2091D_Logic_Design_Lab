module tb_d_flip_flop_RE;

reg D, CLK;
wire Q, Q_BAR;

mod_d_flip_flop_RE DFF(Q, Q_BAR, D, CLK);

initial
begin
    $dumpfile("01a_tb_d_flip_flop_RE.vcd");
    $dumpvars(1, tb_d_flip_flop_RE);

    CLK = 0; #10;
    D = 0; #10;
    CLK = 1; #10;
    D = 1; #10;
    CLK = 0; #10;
    CLK = 1; #10;
    D = 0; #10;
    CLK = 0; #10;
end

endmodule
