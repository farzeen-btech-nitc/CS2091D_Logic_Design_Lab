module mod_DFF_RE_ASYNC_RST_HL(Q, Q_BAR, D, CLK, R);
output Q, Q_BAR;
input D, CLK, R;

wire Q1, Q1_BAR;

wire R_BAR;
wire CLK_BAR;

mod_not not_rst(R_BAR, R);
mod_not not_clk(CLK_BAR, CLK);

mod_d_r_latch d_latch1(Q1, Q1_BAR, D, CLK_BAR, R);
mod_d_r_latch d_latch2(Q, Q_BAR, Q1, CLK, R);

endmodule
