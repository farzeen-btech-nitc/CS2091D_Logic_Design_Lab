module mod_d_flip_flop_RE(Q, Q_BAR, D, CLK);
output Q, Q_BAR;
input D, CLK;

wire CLK_BAR;
mod_not not_clk(CLK_BAR, CLK);

wire Q1, Q1_BAR;

mod_d_latch d_latch1(Q1, Q1_BAR, D, CLK_BAR); //Enable = CLK_BAR
mod_d_latch d_latch2(Q, Q_BAR, Q1, CLK);

endmodule
