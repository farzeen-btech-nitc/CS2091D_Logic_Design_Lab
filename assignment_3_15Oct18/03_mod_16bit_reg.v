module mod_16bit_reg(OUT, IN, LOAD, CLK);
input CLK, LOAD;
input [15:0] IN;
output [15:0] OUT;

mod_binary_cell bin_cell [15:0] (OUT, IN, LOAD, CLK);

endmodule
