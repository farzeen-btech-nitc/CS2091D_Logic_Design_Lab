module tb_ram8;
reg CLK, LOAD;
reg [2:0] ADDR;
reg [15:0] IN;

wire [15:0] OUT;

mod_ram8 ram8(OUT, IN, ADDR, LOAD, CLK);

initial
begin
    $dumpfile("04_tb_ram8.vcd");
    $dumpvars(1, tb_ram8);

    LOAD = 1; #1;
    IN = 16'b0; #1;
    ADDR = 0;
    CLK = 0; #1; CLK = 1; #1; CLK = 0; #1;
    #10;

    LOAD = 0; #1;
    IN = 123; #1;
    CLK = 0; #1; CLK = 1; #1; CLK = 0; #1;
    #10;

    LOAD = 1; #1;
    IN = 123; #1;
    CLK = 0; #1; CLK = 1; #1; CLK = 0; #1;
    #10;

    ADDR = 15;
    CLK = 0; #1; CLK = 1; #1; CLK = 0; #1;
    #10;

end

endmodule
