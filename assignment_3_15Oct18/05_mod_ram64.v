module mod_ram64(OUT, IN, ADDR, LOAD, CLK);
output [15:0] OUT;
input [15:0] IN;
input [5:0] ADDR;
input LOAD, CLK;

wire [127:0] OUT_, LOAD_;


mod_8way_demux demux8[15:0](LOAD_, LOAD, ADDR[5:3]);
mod_16bit_8way_mux mux8(OUT, OUT_, ADDR[5:3]);

mod_ram8 ram8 [7:0] (OUT_, IN, ADDR[2:0], LOAD, CLK);

endmodule
//todo
