module tb_DFF_FE_ASYNC_RST_HL;

reg D, CLK, R;
wire Q, Q_BAR;

mod_DFF_FE_ASYNC_RST_HL DFF(Q, Q_BAR, D, CLK, R);

initial
begin
    $dumpfile("01g_tb_DFF_FE_ASYNC_RST_HL.vcd");
    $dumpvars(1, tb_DFF_FE_ASYNC_RST_HL);

    // Initialize
    R = 0; D = 0; #2;
    CLK = 0; #2; CLK = 1; #2; CLK = 0;
    #10;

    // CLK = 0, wiggle D
    D = 0; #2; D = 1; #2 D = 0;
    #10;

    CLK = 1; #2;
    // CLK = 1, wiggle D
    D = 0; #2; D = 1; #2 D = 0; #2
    #10;

    // Falling edge test, D HIGH
    D = 1; #2; CLK = 0; #2;
    #10;
    // The above test should work and Q should be HIGH now.

    // Rising edge test, D LOW
    D = 0; #2; CLK = 1; #2;
    #10;
    // Q should still be HIGH;

    // Falling edge test, D LOW
    D = 0; #2; CLK = 0; #2;
    #10;
    // The above test should work and Q should be LOW now.

    // Rising edge test, D HIGH
    D = 1; #2; CLK = 1; #2;
    #10;


    // Up Q, check R on next clock edge
    D = 1; #2; CLK = 1; #2; CLK = 0; #2
    R = 1; #2;
    CLK = 1; #2; CLK = 0; #2;
    #10;

end

endmodule
