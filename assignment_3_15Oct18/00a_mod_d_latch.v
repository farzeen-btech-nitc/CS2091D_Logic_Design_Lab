module mod_d_latch(Q, Q_BAR, D, E);
input D, E;
output Q, Q_BAR;

wire D_BAR, S, R;

mod_not not00(D_BAR, D);

mod_and and01(S, D, E);
mod_and and02(R, D_BAR, E);

mod_nor nor01(Q, R, Q_BAR);
mod_nor nor02(Q_BAR, S, Q);

endmodule
