module mod_binary_cell(OUT, IN, LOAD, CLK);
input IN, LOAD, CLK;
output OUT;

wire D;
wire D1, D2;

mod_and and1(D1, LOAD, IN);
mod_and and2(D2, ~LOAD, OUT);
mod_or  or1 (D, D1, D2);

mod_d_flip_flop_RE DFF(OUT, ,D, CLK);

endmodule
