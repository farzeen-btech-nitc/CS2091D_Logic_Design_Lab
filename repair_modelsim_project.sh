i=36
folder=/home/student/CS2091D_Logic_Design_Lab/assignment_2_10Sept18/
list_file=file3.out

for f in $folder/*.v; do 
    echo "Project_File_$i = $f" >>$list_file
    echo "Project_File_P_$i = cover_toggle 0 vlog_protect 0 file_type verilog group_id 0 cover_exttoggle 0 cover_nofec 0 cover_cond 0 vlog_1995compat 0 vlog_nodebug 0 folder $(basename $folder) last_compile 0 cover_fsm 0 cover_branch 0 vlog_noload 0 vlog_enable0In 0 cover_excludedefault 0 vlog_disableopt 0 cover_covercells 0 vlog_hazard 0 vlog_showsource 0 cover_optlevel 3 voptflow 1 ood 1 vlog_0InOptions {} toggle - vlog_options {} compile_to work vlog_upper 0 cover_noshort 0 compile_order 0 dont_compile 0 cover_expr 0 cover_stmt 0" >>$list_file
    i=$((++i))
done
