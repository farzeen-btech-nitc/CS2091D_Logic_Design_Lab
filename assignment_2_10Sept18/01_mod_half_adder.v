module mod_half_adder(out, c_out, in0, in1);
output out, c_out;
input in0, in1;

mod_xor xor1(out, in0, in1);
mod_and and1(c_out, in0, in1);

endmodule
