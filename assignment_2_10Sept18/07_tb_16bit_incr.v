module tb_16bit_incr;
reg [15:0] in;
wire [15:0] out;
wire c_out;

mod_16bit_incr incr(out, c_out, in, 1'b1);

initial begin
    $monitor("IN: %d; OUT: %d; C: %d", in, out, c_out);
    in = 0; #20
    in = 10; #20
    in = 65535; #20
    in = 65536; #20
    ;
end

endmodule

