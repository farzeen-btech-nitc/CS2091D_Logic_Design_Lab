module mod_4bit_adder(out, c_out, in_a, in_b, c_in);
output [3:0] out;
output c_out;
input [3:0] in_a, in_b;
input c_in;

wire [2:0] c_int;

mod_full_adder adder_0(out[0], c_int[0], in_a[0], in_b[0], c_in);
mod_full_adder adder_1(out[1], c_int[1], in_a[1], in_b[1], c_int[0]);
mod_full_adder adder_2(out[2], c_int[2], in_a[2], in_b[2], c_int[1]);
mod_full_adder adder_3(out[3], c_out   , in_a[3], in_b[3], c_int[2]);

endmodule
