module mod_4bit_incr(out, c_out, in, c_in);
output [3:0] out;
input  [3:0] in;
output c_out;
input  c_in;

wire [3:0] c_int;
mod_half_adder half_adder0(out[0], c_int[0], in[0], c_in);
mod_half_adder half_adder1(out[1], c_int[1], in[1], c_int[0]);
mod_half_adder half_adder2(out[2], c_int[2], in[2], c_int[1]);
mod_half_adder half_adder3(out[3], c_out, in[3], c_int[2]);

endmodule
