module tb_half_adder;
reg in0, in1;
wire out, c_out;

mod_half_adder half_adder1(out, c_out, in0, in1);

initial
begin
	$monitor("IN: %d%d OUT: %d, COUT: %d", in1, in0, out, c_out);
	in1 = 0; in0 = 0;
#10	in1 = 0; in0 = 1;
#10	in1 = 1; in0 = 0;
#10	in1 = 1; in0 = 1;
#10	;
end

endmodule

	