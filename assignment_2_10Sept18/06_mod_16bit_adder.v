module mod_16bit_adder(out, c_out, in_a, in_b, c_in);
output [15:0] out;
output c_out;
input  [15:0] in_a, in_b;
input  c_in;

wire [2:0] c_int;

mod_4bit_adder adder0(out[3:0], c_int[0], in_a[3:0], in_b[3:0], c_in);
mod_4bit_adder adder1(out[7:4], c_int[1], in_a[7:4], in_b[7:4], c_int[0]);
mod_4bit_adder adder2(out[11:8], c_int[2], in_a[11:8], in_b[11:8], c_int[1]);
mod_4bit_adder adder3(out[15:12], c_out, in_a[15:12], in_b[15:12], c_int[2]);

endmodule
