module tb_4bit_negator;
reg [3:0] in;
wire out;

mod_4bit_negator negator(out, in);

integer i;
initial begin 
  $monitor("IN: %b; OUT: %b", in, out);
  for(i=0; i<16; i=i+1) begin
    in = i; #10;
  end
end

endmodule
    