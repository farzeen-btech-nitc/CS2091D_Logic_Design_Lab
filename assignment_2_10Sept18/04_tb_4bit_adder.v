module tb_4bit_adder;

reg [3:0] in_a, in_b;
reg c_in;
wire [3:0] out;
wire c_out;

mod_4bit_adder adder(out, c_out, in_a, in_b, c_in);
integer i, j, k;
initial begin
  for(i=0; i<2; i=i+1) begin
    c_in = i;
    for(j=0; j<16; j=j+1) begin
      in_a = j;
      for(k=0; k<16; k=k+1) begin
        in_b = k; #10;
      end
    end
  end
end

endmodule
