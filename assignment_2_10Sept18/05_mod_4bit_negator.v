module mod_4bit_negator(out, in);
output out;
input [3:0] in;

wire n0, n1;

mod_nor nor0(n0, in[1], in[0]);
mod_nor nor1(n1, in[3], in[2]);
mod_and and0(out, n0, n1);

endmodule
