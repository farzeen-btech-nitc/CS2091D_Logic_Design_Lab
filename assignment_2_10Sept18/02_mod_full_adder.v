module mod_full_adder(out, c_out, in0, in1, c_in);
output out, c_out;
input in0, in1, c_in;

wire out_int, c_out0, c_out1;

mod_half_adder half_adder_1(out_int, c_out0, in0, in1);
mod_half_adder half_adder_2(out, c_out1, out_int, c_in);
mod_or or1(c_out, c_out0, c_out1);

endmodule
