module tb_16bit_adder;
reg [15:0] in_a, in_b;
reg c_in;
wire [15:0] out;
wire c_out;

mod_16bit_adder adder(out, c_out, in_a, in_b, c_in);

initial begin
  $monitor("C_IN:%d, IN_A: %d, IN_B: %d, OUT: %d, C_OUT: %d", c_in, in_a, in_b, out, c_out);
  c_in = 0; in_a = 70; in_b = 78; #10;
  c_in = 0; in_a = 186; in_b = 144; #10;
  c_in = 0; in_a = 186; in_b = 255; #10;
  c_in = 0; in_a = 255; in_b = 255; #10;
  c_in = 1; in_a = 70; in_b = 78; #10;
  c_in = 1; in_a = 186; in_b = 144; #10;
  c_in = 1; in_a = 186; in_b = 255; #10;
  c_in = 1; in_a = 255; in_b = 255; #10;
  c_in = 1; in_a = 65534; in_b = 0; #10;
  c_in = 1; in_a = 65535; in_b = 0; #10;
  c_in = 1; in_a = 65535; in_b = 1; #10;
  c_in = 1; in_a = 65535; in_b = 65535; #10;
end

endmodule
