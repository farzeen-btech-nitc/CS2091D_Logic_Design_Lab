module tb_full_adder;
reg in0, in1, c_in;
wire out, c_out;

mod_full_adder full_adder1(out, c_out, in0, in1, c_in);

initial
begin
    $monitor(" IN %d%d; C_IN: %d; OUT: %d; C_OUT: %d;", in1, in0, c_in, out, c_out);
    c_in = 0; in1 = 0; in0 = 0;
    #10 c_in = 0; in1 = 0; in0 = 1;
    #10 c_in = 0; in1 = 1; in0 = 0;
    #10 c_in = 0; in1 = 1; in0 = 1;
    #10 c_in = 1; in1 = 0; in0 = 0;
    #10 c_in = 1; in1 = 0; in0 = 1;
    #10 c_in = 1; in1 = 1; in0 = 0;
    #10 c_in = 1; in1 = 1; in0 = 1;
    #10 ;
end

endmodule
