module mod_16bit_incr(out, c_out, in, c_in);
output [15:0] out;
input  [15:0] in;
output c_out;
input c_in;
wire [2:0] c_int;

mod_4bit_incr incr_3(out[15:12], c_out, in[15:12], c_int[2]);
mod_4bit_incr incr_2(out[11:8], c_int[2], in[11:8], c_int[1]);
mod_4bit_incr incr_1(out[7:4], c_int[1], in[7:4], c_int[0]);
mod_4bit_incr incr_0(out[3:0], c_int[0], in[3:0], c_in);

endmodule
