module tb_4bit_incr;
reg [3:0] in;
wire [3:0] out;
wire c_out;

mod_4bit_incr incr(out, c_out, in, 1'b1);

initial
begin
    $monitor("IN: %b;  OUT: %b, C: %b", in, out, c_out);
    in[3] = 0; in[2] = 0; in[1] = 0; in[0] = 0;
    #10     in[3] = 0; in[2] = 0; in[1] = 0; in[0] = 1;
    #10     in[3] = 0; in[2] = 0; in[1] = 1; in[0] = 0;
    #10     in[3] = 0; in[2] = 0; in[1] = 1; in[0] = 1;
    #10     in[3] = 0; in[2] = 1; in[1] = 0; in[0] = 0;
    #10     in[3] = 0; in[2] = 1; in[1] = 0; in[0] = 1;
    #10     in[3] = 0; in[2] = 1; in[1] = 1; in[0] = 0;
    #10     in[3] = 0; in[2] = 1; in[1] = 1; in[0] = 1;
    #10     in[3] = 1; in[2] = 0; in[1] = 0; in[0] = 0;
    #10     in[3] = 1; in[2] = 0; in[1] = 0; in[0] = 1;
    #10     in[3] = 1; in[2] = 0; in[1] = 1; in[0] = 0;
    #10     in[3] = 1; in[2] = 0; in[1] = 1; in[0] = 1;
    #10     in[3] = 1; in[2] = 1; in[1] = 0; in[0] = 0;
    #10     in[3] = 1; in[2] = 1; in[1] = 0; in[0] = 1;
    #10     in[3] = 1; in[2] = 1; in[1] = 1; in[0] = 0;
    #10     in[3] = 1; in[2] = 1; in[1] = 1; in[0] = 1;
    #10 ;
end

endmodule
