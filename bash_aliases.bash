
function v() {
    if [ ! -f $(echo "$1*tb*.v") ]; then
        echo "No file found for: $1";
        return;
    fi
    iverilog -c library.list $1*tb*.v
    ./a.out
    gtkwave $1*.vcd --rcvar 'enable_vcd_autosave yes' --rcvar 'do_initial_zoom_fit yes' --rcvar 'splash_disable on' --rcvar 'fill_waveform 1'
    rm a.out
    rm $1*.vcd
    rm vcd_autosave.sav
}
