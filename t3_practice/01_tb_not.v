module tb_not;

reg a;
wire b;

initial
begin
	$monitor("a: %d, b: %d", a, b);
	a=0;
#50	a=1;
#50	a=0;
#50	;
end

mod_not not1(b, a);
endmodule
