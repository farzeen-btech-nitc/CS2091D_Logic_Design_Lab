module tb_16bit_not;

reg[15:0] a;
wire[15:0] b;

mod_16bit_not not16(b, a);

initial
begin
	$monitor("a: %b, b: %b", a, b);
	a = 0; 
#10	a = 16'b1111111111111111;
#10	;
end

endmodule
