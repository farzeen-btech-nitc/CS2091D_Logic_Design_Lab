module mod_or(c, a, b);
input a, b;
output c;

wire n_a;
wire n_b;

nand(c, n_a, n_b);
mod_not not1(n_a, a);
mod_not not2(n_b, b);

endmodule
