module tb_x_y;
reg [15:0] x, y;
wire [15:0] out;

mod_x_y xy(out, x, y);

initial
begin
    $dumpfile("21_tb_x_y.vcd");
    $dumpvars(1, tb_x_y);
    x = 6;
    y = 3;

    #10;
end

endmodule
