module tb_xy;
reg [3:0] x, y;
wire [7:0] out;

mod_xy xy(out, x, y);

initial
begin
    $dumpfile("20_tb_xy.vcd");
    $dumpvars(1, tb_xy);
    x = 6;
    y = 5;

    #10;
end

endmodule
