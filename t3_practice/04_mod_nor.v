module mod_nor(c, a, b);
input a, b;
output c;

wire n_c;

mod_or or1(n_c, a, b);
mod_not not1(c, n_c);

endmodule
