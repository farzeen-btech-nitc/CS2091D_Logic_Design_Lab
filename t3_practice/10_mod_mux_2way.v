module mod_mux_2way(out, in1, in2, sel);
input in1, in2,sel;
output out;


wire selb;
not(selb, sel);

wire out1, out2;
and(out1, selb, in1);
and(out2, sel, in2);

or(out, out1, out2);

endmodule
