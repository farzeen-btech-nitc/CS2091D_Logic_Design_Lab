module mod_x_y(out, x, y);
output reg [15:0] out;
input [15:0] x, y;

reg [15:0] temp;
integer i;
initial
begin
    #1;
    {temp, out} = x;
    for(i = 0; i<16; i=i+1) begin
        {temp, out} = {temp, out}* 2;
        $display("temp: %b; out: %b;", temp, out);
        if(temp>=y) begin
            out[0] = 1'b1;
            temp = temp - y;
        end;
        //else begin
        //  out[0] = 1'b0;
        //end;
    end
end


endmodule
