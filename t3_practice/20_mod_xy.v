module mod_xy(out, x, y);
output reg [7:0] out;
input [3:0] x, y;

reg [3:0] out_hi;
reg c;
integer i;
reg [3:0]out_lo;
initial
begin
    out = 0;
    out_hi = 4'b0;
    #1;
    // c = 0;
    out_lo=x;
    for(i = 0; i<4; i=i+1) begin
        $display("%b|%b", out_hi, out_lo);
        c=0;
        if(out_lo[0]) begin
            {c, out_hi} = out_hi + y;
        end
        {out_hi, out_lo} = {c, out_hi, out_lo} >> 1;
    end
    out = {out_hi,out_lo};
end


endmodule
