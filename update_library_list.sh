#!/bin/bash

############################################################################
# Creates a library.list file to be passed to icarus verilog compiler      #
# so that you don't have to specify all library file names when compiling. #
############################################################################

ls *mod*.v >library.list
if [ $? -eq 0 ]; then
    echo "List of modules:"
    cat library.list
    echo
    echo "Library successfully updated."
else
    echo "Error occured."
fi

