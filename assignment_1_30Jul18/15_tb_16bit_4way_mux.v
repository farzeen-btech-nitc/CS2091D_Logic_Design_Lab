module tb_16bit_4way_mux;

reg  [15:0] in0, in1, in2, in3;
reg  sel0, sel1;
wire [15:0] out;

mod_16bit_4way_mux mux(out, in0, in1, in2, in3, sel0, sel1);
initial begin
    $monitor("SEL0: %d, SEL1: %d, IN0: %d, IN1: %d, IN2: %d, IN3: %d, OUT: %d", sel0, sel1, in0, in1, in2, in3, out);
    $dumpfile("15_tb_16bit_4way_mux.vcd");
    $dumpvars(1, tb_16bit_4way_mux);

    #10    sel0 = 0; sel1 = 0;
    in0  = 0; in1  = 0; in2 = 0; in3 = 0;
    #10    sel0 = 0; sel1 = 1;
    in0  = 0; in1  = 0; in2 = 0; in3 = 0;
    #10    sel0 = 1; sel1 = 0;
    in0  = 0; in1  = 0; in2 = 0; in3 = 0;
    #10    sel0 = 1; sel1 = 1;
    in0  = 0; in1  = 0; in2 = 0; in3 = 0;

    #10    sel0 = 0; sel1 = 0;
    in0  = 1; in1  = 0; in2 = 0; in3 = 0;
    #10    sel0 = 0; sel1 = 0;
    in0  = 0; in1  = 1; in2 = 0; in3 = 0;
    #10    sel0 = 0; sel1 = 0;
    in0  = 0; in1  = 0; in2 = 1; in3 = 0;
    #10    sel0 = 0; sel1 = 0;
    in0  = 0; in1  = 0; in2 = 0; in3 = 1;

    #10    sel0 = 0; sel1 = 0;
    in0  = 0; in1  = 1; in2 = 0; in3 = 1;
    #10    sel0 = 1; sel1 = 0;
    in0  = 0; in1  = 1; in2 = 0; in3 = 1;
    #10    ;
end

endmodule

