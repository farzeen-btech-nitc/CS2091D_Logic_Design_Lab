module tb_8way_demux;

wire [7:0] out;
reg in;
reg [2:0] sel;

mod_8way_demux demux(out, in, sel);

initial
begin
    $monitor("IN: %b\nSEL: %b\nOUT: %b", in, sel, out);
    $dumpfile("18_tb_8way_demux.vcd");
    $dumpvars(1, tb_8way_demux);
    in = 0; sel[0] = 0; sel[1] = 0; sel[2] = 0;
    #10 in = 1; sel[0] = 0; sel[1] = 0; sel[2] = 0;
    #10 in = 1; sel[0] = 1; sel[1] = 0; sel[2] = 0;
    #10 in = 1; sel[0] = 0; sel[1] = 1; sel[2] = 0;
    #10 in = 1; sel[0] = 1; sel[1] = 1; sel[2] = 0;

    #10 in = 0; sel[0] = 0; sel[1] = 0; sel[2] = 1;
    #10 in = 1; sel[0] = 0; sel[1] = 0; sel[2] = 1;
    #10 in = 1; sel[0] = 1; sel[1] = 0; sel[2] = 1;
    #10 in = 1; sel[0] = 0; sel[1] = 1; sel[2] = 1;
    #10 in = 1; sel[0] = 1; sel[1] = 1; sel[2] = 1;
    #10 ;
end

endmodule
