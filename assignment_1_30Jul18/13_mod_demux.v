module mod_demux(out0, out1, in, sel);
input sel, in;
output out0, out1;
wire sel_not;

mod_not not0(sel_not, sel);
mod_and and0(out0, in, sel_not);
mod_and and1(out1, in, sel);

endmodule
