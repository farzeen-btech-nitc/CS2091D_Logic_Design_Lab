module tb_xnor;
reg a, b;
wire c;

initial
begin
	$monitor("a: %d; b: %d; c: %d", a, b, c);
	a = 0; b = 0;
#50	a = 0; b = 1;
#50	a = 1; b = 0;
#50 	a = 1; b = 1;
#50	;
end

mod_xnor xnor1(c, a, b);

endmodule
