module mod_8way_demux(out, in, sel);
output [7:0] out;
input in;
input [2:0] sel;

wire in_low, in_high;

mod_4way_demux demux_low(out[0], out[1], out[2], out[3], in_low, sel[0], sel[1]);
mod_4way_demux demux_high(out[4], out[5], out[6], out[7], in_high, sel[0], sel[1]);
mod_demux demux_sel(in_low, in_high, in, sel[2]);

endmodule
