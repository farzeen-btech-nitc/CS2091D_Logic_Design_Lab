module tb_16bit_and;

reg[15:0] a;
reg[15:0] b;
wire[15:0] c;

mod_16bit_and and16(c, a, b);

integer i,j;
initial
begin
	for(i=0; i<=255; i=i+1)
	begin 
		#10 a = i;
		for(j=0; j<=255; j=j+1) #10 b = j;
	end
end

endmodule
