module mod_4way_demux(out0, out1, out2, out3, in, sel0, sel1);
output out0, out1, out2, out3;
input  in, sel0, sel1;

wire in_low, in_high;

mod_demux demux_low(out0, out1, in_low, sel0);
mod_demux demux_high(out2, out3, in_high, sel0);
mod_demux demux_sel(in_low, in_high, in, sel1);

endmodule
