module tb_mod_16bit_8way_mux;

wire [15:0] out;
reg  [7:0][15:0] in;
reg  [2:0] sel;

mod_16bit_8way_mux mux_16bit_8way(out, in, sel);

initial
begin
    $dumpfile("16_tb_16bit_8way_mux.vcd");
    $dumpvars(1, tb_mod_16bit_8way_mux);

    sel[0] = 0; sel[1] = 0; sel[2] = 0;
    in[0] = 1; in[1] = 0; in[2] = 0; in[3] = 0; in[4] = 0; in[5] = 0; in[6] = 0; in[7] = 0;

    #10 sel[0] = 0; sel[1] = 1; sel[2] = 1;
    in[0] = 0; in[1] = 0; in[2] = 0; in[3] = 0; in[4] = 0; in[5] = 0; in[6] = 0; in[7] = 0;
    #10 sel[0] = 0; sel[1] = 1; sel[2] = 1;
    in[0] = 0; in[1] = 0; in[2] = 0; in[3] = 0; in[4] = 0; in[5] = 0; in[6] = 1; in[7] = 0;

    #10 sel[0] = 1; sel[1] = 1; sel[2] = 1;
    in[0] = 0; in[1] = 0; in[2] = 0; in[3] = 0; in[4] = 0; in[5] = 0; in[6] = 0; in[7] = 1;

    #10 sel[0] = 0; sel[1] = 1; sel[2] = 1;
    in[0] = 0; in[1] = 0; in[2] = 0; in[3] = 0; in[4] = 0; in[5] = 0; in[6] = 1 << 2; in[7] = 0;
    #10 sel[0] = 0; sel[1] = 1 << 2; sel[2] = 1 << 2;
    in[0] = 0; in[1] = 0; in[2] = 0; in[3] = 0; in[4] = 0; in[5] = 0; in[6] = 1 << 2; in[7] = 0;
end


endmodule
