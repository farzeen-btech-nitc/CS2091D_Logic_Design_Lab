module tb_mux;

reg in0, in1, sel;
wire out;

mod_mux mux(out, in0, in1, sel);
initial begin
    $monitor("SEL: %d, IN0: %d, IN1: %d, OUT: %d", sel, in0, in1, out);
    $dumpfile("12_tb_mux.vcd");
    $dumpvars(1, tb_mux);
       sel=0; in0 = 0; in1 = 0;
#10    sel=0; in0 = 1; in1 = 0;
#10    sel=0; in0 = 0; in1 = 1;
#10    sel=0; in0 = 1; in1 = 1;
#10    sel=1; in0 = 0; in1 = 0;
#10    sel=1; in0 = 1; in1 = 0;
#10    sel=1; in0 = 0; in1 = 1;
#10    sel=1; in0 = 1; in1 = 1;
#10    ;
end

endmodule

