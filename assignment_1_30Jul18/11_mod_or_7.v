module mod_or_7(out, in_0, in_1, in_2, in_3, 
		     in_4, in_5, in_6, in_7);
output out;
input in_0, in_1, in_2, in_3,
      in_4, in_5, in_6, in_7;

wire[5:0] w;

mod_or or_0(w[0], in_0, in_1);
mod_or or_1(w[1], in_2, w[0]);
mod_or or_2(w[2], in_3, w[1]);
mod_or or_3(w[3], in_4, w[2]);
mod_or or_4(w[4], in_5, w[3]);
mod_or or_5(w[5], in_6, w[4]);
mod_or or_6(out, in_7, w[5]);

endmodule