module mod_16bit_mux(out, in0, in1, sel);
output [15:0]  out;
input  [15:0]  in0, in1;
input  sel;

mod_mux mux_array[15:0](out, in0, in1, sel);

endmodule
