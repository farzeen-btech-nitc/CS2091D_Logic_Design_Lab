module tb_4way_demux;

wire out0, out1, out2, out3;
reg in, sel0, sel1;

mod_4way_demux  demux(out0, out1, out2, out3, in, sel0, sel1);

initial
begin
    $monitor("IN   : %d. SEL0 : %d. SEL1 : %d.\nOUT0 : %d. OUT1 : %d. OUT2 : %d. OUT3 : %d. \n", in, sel0, sel1, out0, out1, out2, out3);
    $dumpfile("17_tb_4way_demux.vcd");
    $dumpvars(1, "tb_4way_demux");
    in = 0; sel0 = 0; sel1 = 0;
#10 in = 1; sel0 = 0; sel1 = 0;
#10 in = 1; sel0 = 1; sel1 = 0;
#10 in = 1; sel0 = 0; sel1 = 1;
#10 in = 1; sel0 = 1; sel1 = 1;
#10 ;
end

endmodule
