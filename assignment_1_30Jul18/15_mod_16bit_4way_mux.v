module mod_16bit_4way_mux(out, in0, in1, in2, in3, sel0, sel1);
output [15:0] out;
input  [15:0] in0, in1, in2, in3;
input  sel0, sel1;

wire   [15:0] out_low, out_high;

mod_16bit_mux mux_low (out_low, in0, in1, sel0);
mod_16bit_mux mux_high(out_high, in2, in3, sel0);
mod_16bit_mux mux_sel (out, out_low, out_high, sel1);

endmodule
