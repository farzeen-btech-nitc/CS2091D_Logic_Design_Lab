module mod_16bit_8way_mux(out, in, sel);
output [15:0] out;
input  [7:0][15:0] in;
input  [2:0] sel;
wire [15:0] out_low, out_high;

mod_16bit_4way_mux mux_low  (out_low, in[0], in[1], in[2], in[3], sel[0], sel[1]);
mod_16bit_4way_mux mux_high (out_high, in[4], in[5], in[6], in[7], sel[0], sel[1]);
mod_16bit_mux      mux_sel  (out, out_low, out_high, sel[2]);

endmodule
