module mod_and(c, a, b);
input a, b;
output c;

wire n_c;

nand(n_c, a, b);
mod_not not1(c, n_c);

endmodule
