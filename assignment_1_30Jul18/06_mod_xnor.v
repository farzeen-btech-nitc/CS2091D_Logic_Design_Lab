module mod_xnor(c, a, b);
//xnor is true when all are true or all are false

input a, b;
output c;

wire n_c;

mod_not not1(c, n_c);
mod_xor xor1(n_c, a, b);

endmodule
