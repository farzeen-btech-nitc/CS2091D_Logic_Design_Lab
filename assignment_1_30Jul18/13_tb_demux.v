module tb_demux;

reg in, sel;
wire out0, out1;

mod_demux mux(out0, out1, in, sel);
initial begin
    $monitor("SEL: %d, IN: %d, OUT0: %d, OUT1: %d", sel, in, out0, out1);
    $dumpfile("13_tb_demux.vcd");
    $dumpvars(1, tb_demux);
       sel=0; in = 0;
#10    sel=0; in = 1;
#10    sel=1; in = 0;
#10    sel=1; in = 1;
#10    ;
end

endmodule

