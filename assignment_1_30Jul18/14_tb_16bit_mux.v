module tb_16bit_mux;

reg [15:0] in0, in1;
reg sel;
wire [15:0] out;

mod_16bit_mux mux_16(out, in0, in1, sel);
initial begin
    $monitor("SEL: %b, IN0: %b, IN1: %b, OUT: %b", sel, in0, in1, out);
    $dumpfile("14_tb_16bit_mux.vcd");
    $dumpvars(1, tb_16bit_mux);
    sel=0; in0 = 0; in1 = 0;
    #10    sel=0; in0 = 1; in1 = 0;
    #10    sel=0; in0 = 0; in1 = 1;
    #10    sel=0; in0 = 1; in1 = 1;
    #10    sel=1; in0 = 0; in1 = 0;
    #10    sel=1; in0 = 1; in1 = 0;
    #10    sel=1; in0 = 0; in1 = 1;
    #10    sel=1; in0 = 1; in1 = 1;

    #10    sel = 0; in0 = 16 'b 00; in1 = 16 'b 00;
    #10    sel = 0; in0 = 16 'b 10; in1 = 16 'b 00;
    #10    sel = 0; in0 = 16 'b 00; in1 = 16 'b 10;
    #10    sel = 0; in0 = 16 'b 10; in1 = 16 'b 10;
    #10    sel = 1; in0 = 16 'b 00; in1 = 16 'b 00;
    #10    sel = 1; in0 = 16 'b 10; in1 = 16 'b 00;
    #10    sel = 1; in0 = 16 'b 00; in1 = 16 'b 10;
    #10    sel = 1; in0 = 16 'b 10; in1 = 16 'b 10;

    #10    sel = 1; in0 = 16 'b 10; in1 = 16 'b 10;
    #10    sel = 1; in0 = 16 'b 11; in1 = 16 'b 10;
    #10    sel = 1; in0 = 16 'b 10; in1 = 16 'b 11;
    #10    sel = 1; in0 = 16 'b 11; in1 = 16 'b 11;
    #10    ;
end

endmodule

