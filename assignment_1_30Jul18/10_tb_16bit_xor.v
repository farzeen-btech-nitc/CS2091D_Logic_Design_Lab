module tb_16bit_xor;

reg[15:0] a;
reg[15:0] b;
wire[15:0] c;

mod_16bit_xor xor16(c, a, b);

initial
begin
	$monitor("a: %b, b: %b, c: %b", a, b, c);
	a = 0; b = 0;
#10	a = 16'b111111111111111; b = 16'b1111111111111111;
#10	a = 0; b = 16'b1111111111111111;
#10	a = 16'b1111111111111111; b = 0;
#10	;
end

endmodule
