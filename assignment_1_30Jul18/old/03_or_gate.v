module or_gate(c, a, b);
    input a, b;
    output c;
    wire n_a, n_b;

    nand(n_a, a, a);
    nand(n_b, b, b);
    nand(c, n_a, n_b);
endmodule

module stimulus;
    reg a, b;
    wire c;

    initial
    begin
        $dumpfile("03_or_gate.vcd");
        $dumpvars(0, a, b, c);
        $monitor(" a: %d, b: %d, c: %d", a, b, c);

    #50    a = 0; b =0;
    #50    a = 1; b =0;
    #50    a = 0; b =1;
    #50    a = 1; b =1;
    #50    ;
    end

    or_gate o1(c, a, b);
endmodule
