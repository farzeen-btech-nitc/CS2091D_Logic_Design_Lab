module and_gate(c, a, b);
    input a, b;
    output c;
    wire d;

    nand(d, a, b);
    nand(c, d, d);
endmodule

module stimulus;
    reg a, b;
    wire c;

    initial
    begin
        $dumpfile("02_and_gate.vcd");
        $dumpvars(0, a, b, c);
        $monitor(" a: %d, b: %d, c: %d", a, b, c);

    #50    a = 0; b =0;
    #50    a = 1; b =0;
    #50    a = 0; b =1;
    #50    a = 1; b =1;
    #50    ;
    end

    and_gate a1(c, a, b);
endmodule
