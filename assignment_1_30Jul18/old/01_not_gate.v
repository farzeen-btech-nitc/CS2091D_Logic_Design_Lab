module not_gate(p, q);
    input q;
    output p;
    nand(p, q,q);
endmodule

module stimulus;
    reg a;
    wire b;

    initial
    begin
        $dumpfile("01_not_gate.vcd");
        $dumpvars(0, a, b);
        $monitor(" a: %d, b: %d", a, b);

    #50    a = 0;
    #50    a = 1;
    #50    ;
    end

    not_gate n1(b,a);
endmodule
