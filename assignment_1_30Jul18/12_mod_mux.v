module mod_mux(out, in0, in1, sel);
input in0, in1, sel;
output out;

wire out0, out1;
wire sel_not;

mod_not not1(sel_not, sel);

mod_and and1(out0, sel_not, in0);
mod_and and2(out1, sel, in1);
mod_or  or1(out, out0, out1);

endmodule
