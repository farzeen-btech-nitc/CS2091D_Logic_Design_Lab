module mod_xor(c, a, b);
input a, b;
output c;

wire n_a, n_b;
mod_not not_a(n_a, a);
mod_not not_b(n_b, b);

wire n_a_b, n_b_a;
mod_and and1(n_a_b, n_a, b);
mod_and and2(n_b_a, n_b, a);

mod_or or1(c, n_a_b, n_b_a);

endmodule
