module tb_or_7;

reg[15:0] a;
wire b;

mod_or_7 or_7(b,a[0], a[1], a[2], a[3],
		a[4], a[5], a[6], a[7]);

initial
begin
	$monitor("a: %b, b: %b", a, b);
	a = 0; 
#10	a = 16'b1111111111111111;
#10	a = 16'b1011110111110111;
#10	a = 16'b0000000000000001;
#10	a = 16'b1000000000000000;
#10	a = 16'b1100000000000000;
#10	;
end

endmodule
